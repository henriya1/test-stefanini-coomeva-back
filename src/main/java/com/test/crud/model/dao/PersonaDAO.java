package com.test.crud.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import com.test.crud.model.entity.Persona;
import java.sql.*;
public class PersonaDAO implements IFPersonaDAO{


	  private Connection cx;
	  
	  public PersonaDAO(){
		  }
	  
	  public PersonaDAO(Connection cx){
		    this.cx=cx;
		  }
    /**
     * Metodo que retorna todos los registros de Persona.
     *
     * @return List
     * @throws PersistenceException
     */
    public List<Persona> loadAll() {
		    List<Persona> personas = null;
		    try{
		    	  personas = this.getListPersona("SELECT * FROM PERSONA WHERE 1 = 1 ");
		          
		      }
		      catch(Exception e){
		      }
		      finally{
		        if(this.cx!=null){
		            try{
		            if(!this.cx.isClosed())
		               this.cx.close();
		            }catch(Exception ex)
		            	{ex.printStackTrace();}
		        } 
		      }
		      
		   
		      return personas;
		  }

    private List<Persona> getListPersona(String sql) throws SQLException {
        List<Persona> personas = new ArrayList<Persona>();
        ResultSet result = null;
        PreparedStatement stmt = null;
        
        try{
        	         cx = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");

          stmt = this.cx.prepareStatement(sql);
          result = stmt.executeQuery();
          while (result.next()) {
        	  personas.add(query(result));
        	 
          }
        }
        catch(Exception e){   
        	
        }
        finally {
              if(result!=null)
                  result.close();
              if(stmt!=null)
                  stmt.close();       
          }          
        
        return personas;
    }
    
    
    /**
     * Metodo que retorna el registro de una Persona.
     *
     * @return List
     * @throws PersistenceException
     */
    public List<Persona> searchPersonaId(Long id) {
		    List<Persona> personaList = new ArrayList<Persona>();
		    StringBuilder sql = new StringBuilder("SELECT * FROM PERSONA WHERE 1=1 AND CODIGO= ?");
		      try{
		    	if(id>0) {
		    		
		    		personaList=this.getListPersonaId(sql.toString(),id);
		    	}
		    	  
		          
		      }
		      catch(Exception e){
		      }
		      finally{
		        if(this.cx!=null){
		            try{
		            if(!this.cx.isClosed())
		               this.cx.close();
		            }catch(Exception ex)
		            	{ex.printStackTrace();}
		        } 
		      }
		      
		   
		      return personaList;
		  }
    
    private List<Persona> getListPersonaId(String sql, Long id) throws SQLException {
        List<Persona> personas = new ArrayList<Persona>();
        ResultSet result = null;
        PreparedStatement stmt = null;
        
        try{
          cx = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
          stmt = this.cx.prepareStatement(sql);
          stmt.setLong(1, id);

          result = stmt.executeQuery();
          while (result.next()) {
        	  personas.add(query(result));
        	 
          }
        }
        catch(Exception e){   
        	
        }
        finally {
              if(result!=null)
                  result.close();
              if(stmt!=null)
                  stmt.close();       
          }          
        
        return personas;
    }

    /**
     * Metodo que Crea el registro de Una Persona.
     *
     * @return Persona
     * @throws PersistenceException
     */
    public Persona savePersona(Persona per) {
		    Persona persona = null;
		    StringBuilder sql = new StringBuilder("INSERT INTO PERSONA (CODIGO,NOMBRE ,APELLIDO ,FECHA_NACIMIENTO ,USERNAME ,PASSWORD ,IDENTIFICACION ,CODI_TIPO_IDENTIFICACION ,CODIGO_ESTADO ) VALUES (?,?,?,?,?,?,?,?,?)");
		      try{

		    	  
		    	  persona=this.getSavePersona(sql.toString(),per);
		    	
		      }
		      catch(Exception e){
		      }
		      finally{
		        if(this.cx!=null){
		            try{
		            if(!this.cx.isClosed())
		               this.cx.close();
		            }catch(Exception ex)
		            	{ex.printStackTrace();}
		        } 
		      }
		      
		   
		      return persona;
		  }
    
    /**
     * Metodo que crea el registro de una Persona.
     * @return Persona
     */
    private Persona getSavePersona(String sql, Persona per) throws SQLException {
        Persona persona = new Persona();
        int result = 0;
        PreparedStatement stmt = null;
        
        try{
        	         cx = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
        	          

          stmt = this.cx.prepareStatement(sql);
          
          stmt.setLong(1,per.getCodigo());
          stmt.setString(2,per.getNombre());
          stmt.setString(3,per.getApellido());
          stmt.setDate(4,per.getFechaNacimiento());
          stmt.setString(5,per.getUserName());
          stmt.setString(6,per.getPassword());
          stmt.setLong(7,per.getIdentificacion());
          stmt.setLong(8,per.getCodiTipoIdentificacion());
          stmt.setLong(9,per.getCodigoEstado());


          result = stmt.executeUpdate();
        	if(result>0)  
          persona=per;
        	 
          
        }
        catch(Exception e){   
        	
        }
        finally {
              if(result!=1)
                  cx.close();
              if(stmt!=null)
                  stmt.close();       
          }          
        
        return persona;
    }
    
    /**
     * Metodo que elimina el registro de una Persona.
     *
     * @return String
     * @throws PersistenceException
     */
    public String deletePersona(Long id) {
		    String persona = null;
		    StringBuilder sql = new StringBuilder("DELETE FROM PERSONA  WHERE CODIGO = ?");
		      try{

		    	  persona=this.getDeletePersona(sql.toString(),id);
		    	
		    	  
		          
		      }
		      catch(Exception e){
		      }
		      finally{
		        if(this.cx!=null){
		            try{
		            if(!this.cx.isClosed())
		               this.cx.close();
		            }catch(Exception ex)
		            	{ex.printStackTrace();}
		        } 
		      }
		      
		   
		      return persona;
		  }
    
    private String getDeletePersona(String sql, Long id) throws SQLException {
        String persona = null;
        int result = 0;
        PreparedStatement stmt = null;
        
        try{
        	         cx = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
        	          

          stmt = this.cx.prepareStatement(sql);
          if(id!=null) {
        	  stmt.setLong(1,id);
              result = stmt.executeUpdate();
              if(result>0)  
          		persona="Registro Eliminado con éxito";
          }


        	
        	 
          
        }
        catch(Exception e){   
        	
        }
        finally {
              if(result!=1)
            		persona="Registro NO Eliminado";

                  cx.close();
              if(stmt!=null)
                  stmt.close();       
          }          
        
        return persona;
    }
    
    /**
     * Metodo que Actualiza el registro de Una Persona.
     *
     * @return Persona
     * @throws PersistenceException
     */
    public Persona updatePersona(Persona per) {
		    Persona persona = null;
		    StringBuilder sql = new StringBuilder("UPDATE PERSONA SET NOMBRE = ?,APELLIDO = ?,FECHA_NACIMIENTO = ?,USERNAME = ?,PASSWORD = ?,IDENTIFICACION = ?,CODI_TIPO_IDENTIFICACION = ?,CODIGO_ESTADO = ? WHERE CODIGO= ?");
		      try{

		    	  
		    	  persona=this.getUpdatePersona(sql.toString(),per);
		    	
		      }
		      catch(Exception e){
		      }
		      finally{
		        if(this.cx!=null){
		            try{
		            if(!this.cx.isClosed())
		               this.cx.close();
		            }catch(Exception ex)
		            	{ex.printStackTrace();}
		        } 
		      }
		      
		   
		      return persona;
		  }
    
    /**
     * Metodo que crea el registro de una Persona.
     * @return Persona
     */
    private Persona getUpdatePersona(String sql, Persona per) throws SQLException {
        Persona persona = new Persona();
        int result = 0;
        PreparedStatement stmt = null;
        
        try{
        	         cx = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
        	          

          stmt = this.cx.prepareStatement(sql);
          
          stmt.setString(1,per.getNombre());
          stmt.setString(2,per.getApellido());
          stmt.setDate(3,per.getFechaNacimiento());
          stmt.setString(4,per.getUserName());
          stmt.setString(5,per.getPassword());
          stmt.setLong(6,per.getIdentificacion());
          stmt.setLong(7,per.getCodiTipoIdentificacion());
          stmt.setLong(8,per.getCodigoEstado());
          stmt.setLong(9,per.getCodigo());


          result = stmt.executeUpdate();
        	if(result>0)  
          persona=per;
        	 
          
        }
        catch(Exception e){   
        	
        }
        finally {
              if(result!=1)
                  cx.close();
              if(stmt!=null)
                  stmt.close();       
          }          
        
        return persona;
    }
    
    public Persona query(ResultSet result) {
    	
    	 Persona persona = new Persona();
   	  try {
		  persona.setCodigo(result.getLong("CODIGO"));
		  persona.setNombre(result.getString("NOMBRE"));
	   	  persona.setApellido(result.getString("APELLIDO"));
	   	  persona.setFechaNacimiento(result.getDate("FECHA_NACIMIENTO"));
	   	  persona.setUserName(result.getString("USERNAME"));
	   	  persona.setPassword(result.getString("PASSWORD"));
	   	  persona.setIdentificacion(result.getLong("IDENTIFICACION"));
	   	  persona.setCodiTipoIdentificacion(result.getLong("CODI_TIPO_IDENTIFICACION"));
	   	  persona.setCodigoEstado(result.getLong("CODIGO_ESTADO"));
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	  
   	  
   	  return persona;
    }
  
}
