package com.test.crud.model.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import com.test.crud.model.entity.Persona;

public interface IFPersonaDAO {

 
    
    /**
     * Metodo que retorna todos los registros de Persona.
     *
     * @return List
     * @throws PersistenceException
     */
    List<Persona> loadAll() throws PersistenceException;
    
    List<Persona> searchPersonaId(Long id) throws PersistenceException;

    Persona savePersona(Persona per) throws PersistenceException;
    
    String deletePersona(Long id) throws PersistenceException;

    Persona updatePersona(Persona per) throws PersistenceException;

}
