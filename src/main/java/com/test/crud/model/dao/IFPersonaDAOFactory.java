package com.test.crud.model.dao;

public interface IFPersonaDAOFactory {

    /**
     * M�todo que nos devuelve una referencia del DAO si ya existe o crea un
     * nuevo DAO cuando no exista y retorna la variable de referencia del nuevo
     * DAO.
     *
     * @return IFPeresonaDAO
     */
    IFPersonaDAO getPersonaDAO();
    
}
