package com.test.crud.model.dao;

public class PersonaDAOFactoryImpl implements IFPersonaDAOFactory{

    private static PersonaDAOFactoryImpl personaDAOFactoryImpl;
    private IFPersonaDAO iFPersonaDAO;
    
    public PersonaDAOFactoryImpl() {
		super();
	}

    /**
     * Metodo que nos devuelve una referencia del DAO si ya existe o crea un
     * nuevo DAO cuando no exista y retorna la variable de referencia del nuevo
     * DAO.
     *
     * @return PersonaDAOFactoryImpl
     */
    public static PersonaDAOFactoryImpl getInstance() {
        if (personaDAOFactoryImpl == null) {
        	personaDAOFactoryImpl = new PersonaDAOFactoryImpl();
        }
        return personaDAOFactoryImpl;
    }
    
    /**
     * Metodo que nos devuelve una referencia del DAO si ya existe o crea un
     * nuevo DAO cuando no exista y retorna la variable de referencia del nuevo
     * DAO.
     *
     * @return IFPersonaDAO
     */
    public IFPersonaDAO getPersonaDAO() {
        if (this.iFPersonaDAO == null) {
            this.iFPersonaDAO = new PersonaDAO();
        }
        return this.iFPersonaDAO;
    }

	
}
