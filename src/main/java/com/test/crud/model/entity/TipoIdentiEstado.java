package com.test.crud.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;



@Entity
@Table(name="Objeto_Padre")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class TipoIdentiEstado {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="CODIGO", nullable=false)
	private Long codigo;
	@Column(name="NOMBRE", nullable=false, length = 20)
	private String nombre;
	@Column(name="FECHA_CREACION")
	private Timestamp fechaCreacion;
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	@Column(name="FECHA_MODIFICACION")
	private Timestamp fechaModificacion;
	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;
	
	
	
	public TipoIdentiEstado() {
		
	}
	
	public TipoIdentiEstado(Long codigo, String nombre, Timestamp fechaCreacion, String usuarioCreacion, Timestamp fechaModificacion,
			String usuarioModificacion) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuarioModificacion = usuarioModificacion;
	}
	

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Timestamp getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
}
