package com.test.crud.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="persona")
public class Persona{
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long codigo;
	@Column(name="NOMBRE", nullable=false, length = 100)
	private String nombre;
	@Column(name="APELLIDO", nullable=false, length = 100)
	private String apellido;
	@Column(name="FECHA_NACIMIENTO", nullable=false)
	private Date fechaNacimiento;
	@Column(name="USERNAME", nullable=false, length = 20)
	private String userName;
	@Column(name="PASSWORD", nullable=false, length = 50)
	private String password;
	@Column(name="IDENTIFICACION", nullable=false)
	private Long identificacion;
	
	@Column(name="CODI_TIPO_IDENTIFICACION", nullable=false)
    private Long codiTipoIdentificacion;
	@Column(name="CODIGO_ESTADO", nullable=false)
	private Long codigoEstado;
	
	@JoinColumn(name = "codigo")
    @OneToOne(fetch = FetchType.LAZY)
    private Estado estado;
	
	@JoinColumn(name = "codigo")
    @OneToOne(fetch = FetchType.LAZY)
    private TipoIdentificacion codi_tipo_identificacion;	
	public Persona() {
		
	}
	
	public Persona(Long codigo, String nombre, String apellido, Date fechaNacimiento, String userName, String password,
			Long identificacion, Long codiTipoIdentificacion, Long codigoEstado) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
		this.userName = userName;
		this.password = password;
		this.identificacion = identificacion;
		this.codiTipoIdentificacion = codiTipoIdentificacion;
		this.codigoEstado = codigoEstado;
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(Long identificacion) {
		this.identificacion = identificacion;
	}

	public Long getCodiTipoIdentificacion() {
		return codiTipoIdentificacion;
	}

	public void setCodiTipoIdentificacion(Long codiTipoIdentificacion) {
		this.codiTipoIdentificacion = codiTipoIdentificacion;
	}

	public Long getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(Long codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

		
}
