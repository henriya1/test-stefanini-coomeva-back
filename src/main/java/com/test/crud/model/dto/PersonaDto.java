package com.test.crud.model.dto;

import java.io.Serializable;
import java.sql.Date;

import com.test.crud.model.entity.Persona;



public class PersonaDto implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4695095753868177446L;
	
	private Long codigo;
	private String nombre;
	private String apellido;
	private Date fechaNacimiento;
	private String userName;
	private String password;
	private Long identificacion;
	private Long codiTipoIdentificacion;
	private Long codigoEstado;
	
	
	public PersonaDto(Long codigo, String nombre, String apellido, Date fechaNacimiento, String userName,String password,
			Long identificacion, Long codiTipoIdentificacion, Long codigoEstado) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
		this.userName = userName;
		this.password= password;
		this.identificacion = identificacion;
		this.codiTipoIdentificacion = codiTipoIdentificacion;
		this.codigoEstado = codigoEstado;
	}
	
	public PersonaDto(Persona per) {
		this.codigo = per.getCodigo();
		this.nombre = per.getNombre();
		this.apellido = per.getApellido();
		this.fechaNacimiento = per.getFechaNacimiento();
		this.userName = per.getUserName();
		this.password= per.getPassword();
		this.identificacion = per.getIdentificacion();
		this.codiTipoIdentificacion = per.getCodiTipoIdentificacion();
		this.codigoEstado = per.getCodigoEstado();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public PersonaDto() {
	}
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(Long identificacion) {
		this.identificacion = identificacion;
	}
	public Long getCodiTipoIdentificacion() {
		return codiTipoIdentificacion;
	}
	public void setCodiTipoIdentificacion(Long codiTipoIdentificacion) {
		this.codiTipoIdentificacion = codiTipoIdentificacion;
	}
	public Long getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(Long codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	
}
