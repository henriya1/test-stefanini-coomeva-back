package com.test.crud.model.dto;

import java.sql.Timestamp;

import javax.persistence.Column;

import com.test.crud.model.entity.TipoIdentiEstado;

public class TipoIdentiEstadoDto {

	public TipoIdentiEstadoDto(Long codigo, String nombre, Timestamp fechaCreacion, String usuarioCreacion,
			Timestamp fechaModificacion, String usuarioModificacion) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.fechaCreacion = fechaCreacion;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuarioModificacion = usuarioModificacion;
	}
	private Long codigo;
	@Column(name="NOMBRE", nullable=false, length = 20)
	private String nombre;
	@Column(name="FECHA_CREACION")
	private Timestamp fechaCreacion;
	@Column(name="USUARIO_CREACION")
	private String usuarioCreacion;
	@Column(name="FECHA_MODIFICACION")
	private Timestamp fechaModificacion;
	@Column(name="USUARIO_MODIFICACION")
	private String usuarioModificacion;
	
	
	
	public TipoIdentiEstadoDto() {

	}
	public TipoIdentiEstadoDto(TipoIdentiEstado identiEstado) {
		super();
		this.codigo = identiEstado.getCodigo();
		this.nombre = identiEstado.getNombre();
		this.fechaCreacion = identiEstado.getFechaCreacion();
		this.usuarioCreacion = identiEstado.getUsuarioCreacion();
		this.fechaModificacion = identiEstado.getFechaModificacion();
		this.usuarioModificacion = identiEstado.getUsuarioModificacion();
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Timestamp getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	
}
