package com.test.crud.controller.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import static java.util.Collections.emptyList;

public class JwtUtil {

	static void addAuthentication(HttpServletResponse res, String usuario) {
		
		String token = Jwts.builder().setSubject(usuario).signWith(SignatureAlgorithm.HS512, "TestCoomeva").compact();
		
		res.addHeader(usuario, "Bearer "+token);
	}
	
	static Authentication getAuthentication(HttpServletRequest req) {
		
		String token = req.getHeader("Authorization");
		
		if(token!=null) {
			String usuario = Jwts.parser().setSigningKey("TestCoomeva").parseClaimsJws(token.replace("Bearer", "")).
					getBody().getSubject();
			
			return usuario != null ? new UsernamePasswordAuthenticationToken(usuario, null, emptyList()):null;
			
			
		}
		
		return null;

		
	}
}
