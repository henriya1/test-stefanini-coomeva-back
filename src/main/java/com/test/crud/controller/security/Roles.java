package com.test.crud.controller.security;

import java.io.Serializable;

public class Roles implements Serializable{
	
	
	private static final long serialVersionUID = -1880324652568690216L;
	
	private String usuario;
	private String password;
	private byte rol;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public byte getRol() {
		return rol;
	}
	public void setRol(byte rol) {
		this.rol = rol;
	}

}
