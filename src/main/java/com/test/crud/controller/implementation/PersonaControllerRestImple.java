package com.test.crud.controller.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.crud.controller.interfaces.IFPersonaControllerRest;
import com.test.crud.model.dto.PersonaDto;
import com.test.crud.model.entity.Persona;
import com.test.crud.service.interfaces.IFPersonaService;
@RestController
@CrossOrigin(origins= {"http://localhost:4200"})
public class PersonaControllerRestImple implements IFPersonaControllerRest{

	PersonaDto personaDTO = new PersonaDto();
	
	@Autowired
	private IFPersonaService estService;
	
	
	@GetMapping("/api/getAllPersonas")
	public  List<PersonaDto> searchPersonaList(){
		
		List<Persona> personas = new ArrayList<Persona>();
        List<PersonaDto> persona = new ArrayList<PersonaDto>();

        //Response  response  = authServicio.comprobarAuth(auth);

		personas=estService.searchPersonaList();
        for(Persona per:personas) {
    		PersonaDto personaDto = new PersonaDto(per);

        	persona.add(personaDto);
        }
		return persona;
	}
	
	@GetMapping("/api/getPersona/{id}")
	public   List<PersonaDto>  searchPersonaId(@Validated  @PathVariable Long id){
		List<Persona> personas = new ArrayList<Persona>();
        List<PersonaDto> persona = new ArrayList<PersonaDto>();
        
        personas=estService.searchPersonaId(id);
        for(Persona per:personas) {
    		PersonaDto personaDto = new PersonaDto(per);

        	persona.add(personaDto);
        }
		return persona;
	}
	
	@PostMapping("/api/postAddPersona")
	public  PersonaDto savePersona(@RequestBody Persona per){
		
		PersonaDto personaDto = new PersonaDto(estService.savePersona(per));
		return personaDto;
	}
	
	@DeleteMapping("/api/deletePersonaId/{id}")
	public  String deletePersonaId(@PathVariable Long id){
		return estService.deletePersonaId(id);
	}
	
	@PutMapping("/api/updatePersona")
	public  PersonaDto updatePersona(@RequestBody Persona per){
		PersonaDto personaDto = new PersonaDto(estService.updatePersona(per));
		return personaDto;
	}
	
	
	 @ExceptionHandler(NoSuchElementException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  public ResponseEntity<String> handleNoSuchElementFoundException(
	      NoSuchElementException exception
	  ) {
	    return ResponseEntity
	        .status(HttpStatus.NOT_FOUND)
	        .body(exception.getMessage());
	  }
	
	
	
}
