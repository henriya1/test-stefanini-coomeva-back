package com.test.crud.controller.implementation;

import java.util.ArrayList;
import java.util.List;

import com.test.crud.controller.interfaces.IFEstadoControllerRest;

import com.test.crud.model.dto.TipoIdentiEstadoDto;
import com.test.crud.model.entity.Estado;
import com.test.crud.service.interfaces.IFEstadoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins= {"http://localhost:4200"})
public class EstadoControllerRestImple implements IFEstadoControllerRest{
	
	@Autowired
	private IFEstadoService estadoSer;
	
	@GetMapping("/api/getAllEstado")
	public  List<TipoIdentiEstadoDto> searchEstadoList(){
		
		List<Estado> estado = new ArrayList<Estado>();
        List<TipoIdentiEstadoDto> estadoDto = new ArrayList<TipoIdentiEstadoDto>();

        estado=estadoSer.searchEstadoList();
        for(Estado est:estado) {
        	TipoIdentiEstadoDto estDto = new TipoIdentiEstadoDto(est);

        	estadoDto.add(estDto);
        }
		
		return estadoDto;
		
	}
	
	@GetMapping("/api/getIdEstado/{id}")
	public  TipoIdentiEstadoDto searchIdEstado(@PathVariable Long id){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(estadoSer.searchEstadoId(id).get());
		
		return tipoEstDto;
	}
	
	@PostMapping("/api/postAddEstado")
	public  TipoIdentiEstadoDto saveEstado(@RequestBody Estado est){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(estadoSer.saveEstado(est));
		return tipoEstDto;
	}
	
	@DeleteMapping("/api/deleteEstadoId/{id}")
	public  String deleteEstadoId(@PathVariable Long id){
		return estadoSer.deleteEstadoId(id);
	}
	
	@PutMapping("/api/putEstado")
	public  TipoIdentiEstadoDto updateEstado(@RequestBody Estado est){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(estadoSer.saveEstado(est));

		return tipoEstDto;
	}
	
}
