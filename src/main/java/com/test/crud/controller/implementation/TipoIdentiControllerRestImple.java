package com.test.crud.controller.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.crud.controller.interfaces.IFTipoIdentiControlerRest;
import com.test.crud.model.dto.TipoIdentiEstadoDto;
import com.test.crud.model.entity.TipoIdentificacion;
import com.test.crud.service.interfaces.IFTipoIdentificacionService;

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
public class TipoIdentiControllerRestImple implements IFTipoIdentiControlerRest{

	@Autowired	
	private IFTipoIdentificacionService tipoSer;
	
	
	@GetMapping("/api/getAllTipoIdentificacion")
	public  List<TipoIdentiEstadoDto> searchTipoIdentificacionList(){
		
		List<TipoIdentificacion> tipoIden = new ArrayList<TipoIdentificacion>();
        List<TipoIdentiEstadoDto> estadoDto = new ArrayList<TipoIdentiEstadoDto>();

        tipoIden=tipoSer.searchTipoIdentificacionList();
        for(TipoIdentificacion tipo:tipoIden) {
        	TipoIdentiEstadoDto tipoIdenDto = new TipoIdentiEstadoDto(tipo);

        	estadoDto.add(tipoIdenDto);
        }
		
		return estadoDto;
	}
	
	@GetMapping("/api/getIdTipoIdentificacion/{id}")
	public  TipoIdentiEstadoDto searchTipoIdentificacionId(@PathVariable Long id){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(tipoSer.searchTipoIdentificacionId(id).get());

		return tipoEstDto;
	}

	@PostMapping("/api/postAddTipoIdentificacion")
	public  TipoIdentiEstadoDto saveTipoIdentificacion(@RequestBody TipoIdentificacion tipo){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(tipoSer.saveTipoIdentificacion(tipo));

		return tipoEstDto;
	}
	
	@DeleteMapping("/api/deleteTipoIdentificacionId/{id}")
	public  String deleteTipoIdentificacionId(@PathVariable Long id){
		return tipoSer.deleteTipoIdentificacionId(id);
	}

	@PutMapping("/api/putTipoIdentificacion")
	public  TipoIdentiEstadoDto updateTipoIdentificacion(@RequestBody TipoIdentificacion tipo){
		TipoIdentiEstadoDto tipoEstDto = new TipoIdentiEstadoDto(tipoSer.saveTipoIdentificacion(tipo));

		return tipoEstDto;
	}
	
}
