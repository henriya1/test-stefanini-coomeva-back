package com.test.crud.controller.interfaces;

import java.util.List;

import com.test.crud.model.dto.TipoIdentiEstadoDto;
import com.test.crud.model.entity.TipoIdentificacion;

public interface IFTipoIdentiControlerRest {
	
	List<TipoIdentiEstadoDto> searchTipoIdentificacionList();
	
	TipoIdentiEstadoDto searchTipoIdentificacionId(Long id);
	
	TipoIdentiEstadoDto saveTipoIdentificacion(TipoIdentificacion est);
	
	String deleteTipoIdentificacionId(Long id);
	
	TipoIdentiEstadoDto updateTipoIdentificacion(TipoIdentificacion est);

	
}
