package com.test.crud.controller.interfaces;

import java.util.List;

import com.test.crud.model.dto.TipoIdentiEstadoDto;
import com.test.crud.model.entity.Estado;


public interface IFEstadoControllerRest{

	List<TipoIdentiEstadoDto> searchEstadoList();
	
	TipoIdentiEstadoDto searchIdEstado(Long id);
	
	TipoIdentiEstadoDto saveEstado(Estado est);
	
	String deleteEstadoId(Long id);
	
	TipoIdentiEstadoDto updateEstado(Estado est);

}
