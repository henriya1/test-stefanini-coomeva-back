package com.test.crud.controller.interfaces;

import java.util.List;

import com.test.crud.model.dto.PersonaDto;
import com.test.crud.model.entity.Persona;

public interface IFPersonaControllerRest {

	List<PersonaDto> searchPersonaList();
	
	List<PersonaDto>   searchPersonaId(Long id);
	
	PersonaDto savePersona(Persona per);
	
	String deletePersonaId(Long id);	

	PersonaDto updatePersona(Persona per);
		
}
