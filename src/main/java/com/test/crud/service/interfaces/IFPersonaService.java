package com.test.crud.service.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import com.test.crud.model.entity.Persona;

@Service
public interface IFPersonaService {
	
	List<Persona> searchPersonaList();
	
	List<Persona>  searchPersonaId(Long id);

	Persona savePersona(Persona per);
	
	String deletePersonaId(Long id);	

	Persona updatePersona(Persona per);
}
