package com.test.crud.service.interfaces;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
@Component
public interface IFRolesService extends UserDetailsService {
	
	public  UserDetails loadUserByUsername(String usuario);

	
	public List<GrantedAuthority> construGrante(byte rol);
}
