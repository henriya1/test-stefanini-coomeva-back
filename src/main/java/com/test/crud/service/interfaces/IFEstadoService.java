package com.test.crud.service.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.test.crud.model.entity.Estado;

@Service
public interface IFEstadoService {
	
	List<Estado> searchEstadoList();
	
	Optional<Estado>  searchEstadoId(Long id);
	
	Estado saveEstado(Estado est);

	
	String deleteEstadoId(Long id);
	
	Estado updateEstado(Estado est);
	
}
