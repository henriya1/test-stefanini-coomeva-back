package com.test.crud.service.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.test.crud.model.entity.TipoIdentificacion;

@Service
public interface IFTipoIdentificacionService {

List<TipoIdentificacion> searchTipoIdentificacionList();
	
	Optional<TipoIdentificacion>  searchTipoIdentificacionId(Long id);
	
	TipoIdentificacion saveTipoIdentificacion(TipoIdentificacion tipo);

	
	String deleteTipoIdentificacionId(Long id);
	
	TipoIdentificacion updateTipoIdentificacion(TipoIdentificacion tipo);
	
}
