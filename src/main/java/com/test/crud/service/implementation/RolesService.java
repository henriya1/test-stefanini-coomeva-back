package com.test.crud.service.implementation;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.test.crud.model.entity.Roles;
import com.test.crud.repository.IFRolesRepository;
import com.test.crud.service.interfaces.IFRolesService;

@Service
@SpringBootApplication
public class RolesService implements IFRolesService{

	
	public RolesService() {
	}

	@Autowired
	@Qualifier("iFRolesRepository")
	private IFRolesRepository rolRepo;
	
	@Override
	public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException{
		
		Roles usu =rolRepo.findByUsuario(usuario);
		return new User(usu.getUsuario(), usu.getPassword(), usu.isActivo(), usu.isActivo(),  usu.isActivo(),  usu.isActivo(), construGrante(usu.getRol()));
		
	}
	
	
	public List<GrantedAuthority> construGrante(byte rol){
	String[] roles = {"ADMIN","MODIFI"};
	List<GrantedAuthority> autList = new ArrayList<>();
		
	for(int i=0;i<rol;i++) {
		
		autList.add(new SimpleGrantedAuthority(roles[i]));
		
	}
		return autList;
	}
	
	public UserDetails rolByUser(String usuario) {
		return null;
	}
}
