package com.test.crud.service.implementation;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.crud.model.entity.Estado;
import com.test.crud.repository.IFEstadoRepository;
import com.test.crud.service.interfaces.IFEstadoService;

@Service
@Transactional
public class EstadoServiceImple implements IFEstadoService{

	@Autowired
	private IFEstadoRepository ifEstadoRepo;
	
	public  List<Estado> searchEstadoList(){

		return ifEstadoRepo.findAll();
	}
	
	public Optional<Estado>  searchEstadoId(Long id){
		
		 return ifEstadoRepo.findById(id);
	}
	
	public  Estado saveEstado(Estado esta){

		return ifEstadoRepo.save(esta);
	}
	
	public  String deleteEstadoId(Long id){
		String resultado;
		
		try {
			ifEstadoRepo.deleteById(id);
			resultado="Registro elimminado con éxito.";
		}catch(Exception e){
			resultado="No se pudo eliminar el registro.";
		}
			
		return resultado;
		 
	}
	
	public  Estado updateEstado(Estado est){

		return ifEstadoRepo.save(est);
	}
}
