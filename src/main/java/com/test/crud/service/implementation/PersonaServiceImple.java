package com.test.crud.service.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.test.crud.model.dao.IFPersonaDAO;
import com.test.crud.model.dao.IFPersonaDAOFactory;
import com.test.crud.model.dao.PersonaDAOFactoryImpl;
import com.test.crud.model.entity.Persona;
import com.test.crud.service.interfaces.IFPersonaService;

@Service
@Transactional
public class PersonaServiceImple implements IFPersonaService{

	
	public  List<Persona> searchPersonaList(){
        
        List<Persona> listPersona = null;
		IFPersonaDAOFactory iFPersonaDAOFactory = null;
        IFPersonaDAO iFPersonaDAO = null;
        

        try {
            iFPersonaDAOFactory = PersonaDAOFactoryImpl.getInstance();
            iFPersonaDAO = iFPersonaDAOFactory.getPersonaDAO();
            listPersona=iFPersonaDAO.loadAll();
		}catch(PersistenceException ex){
			
		}
		return listPersona;

	}
	
public  List<Persona> searchPersonaId(Long id){
        
        List<Persona> persona = new ArrayList<Persona>();
		IFPersonaDAOFactory iFPersonaDAOFactory = null;
        IFPersonaDAO iFPersonaDAO = null;
        

        try {
            iFPersonaDAOFactory = PersonaDAOFactoryImpl.getInstance();
            iFPersonaDAO = iFPersonaDAOFactory.getPersonaDAO();
            persona=iFPersonaDAO.searchPersonaId(id);
		}catch(PersistenceException ex){
			
		}
		return persona;

	}
	
public  Persona savePersona(Persona per){
    
    Persona persona = new Persona();
	IFPersonaDAOFactory iFPersonaDAOFactory = null;
    IFPersonaDAO iFPersonaDAO = null;
    

    try {
        iFPersonaDAOFactory = PersonaDAOFactoryImpl.getInstance();
        iFPersonaDAO = iFPersonaDAOFactory.getPersonaDAO();
        persona=iFPersonaDAO.savePersona(per);
	}catch(PersistenceException ex){
		
	}
	return persona;

}

public  String deletePersonaId(Long id){
    
    String res = null;
	IFPersonaDAOFactory iFPersonaDAOFactory = null;
    IFPersonaDAO iFPersonaDAO = null;
    

    try {
        iFPersonaDAOFactory = PersonaDAOFactoryImpl.getInstance();
        iFPersonaDAO = iFPersonaDAOFactory.getPersonaDAO();
        res=iFPersonaDAO.deletePersona(id);
	}catch(PersistenceException ex){
		
	}
	return res;

}
public  Persona updatePersona(Persona per){
    
    Persona persona = new Persona();
	IFPersonaDAOFactory iFPersonaDAOFactory = null;
    IFPersonaDAO iFPersonaDAO = null;
    

    try {
        iFPersonaDAOFactory = PersonaDAOFactoryImpl.getInstance();
        iFPersonaDAO = iFPersonaDAOFactory.getPersonaDAO();
        persona=iFPersonaDAO.updatePersona(per);
	}catch(PersistenceException ex){
		
	}
	return persona;

}


}
