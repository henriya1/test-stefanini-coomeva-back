package com.test.crud.service.implementation;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.crud.model.entity.TipoIdentificacion;
import com.test.crud.repository.IFTipoIdentificacion;
import com.test.crud.service.interfaces.IFTipoIdentificacionService;

@Service
@Transactional
public class TipoIdentificacionServiceImple implements IFTipoIdentificacionService{
	
	@Autowired
	private IFTipoIdentificacion ifTipoRepo;
	
	public  List<TipoIdentificacion> searchTipoIdentificacionList(){

		return ifTipoRepo.findAll();
	}
	
	public Optional<TipoIdentificacion>  searchTipoIdentificacionId(Long id){

		return ifTipoRepo.findById(id);
		 
	}
	
	public  TipoIdentificacion saveTipoIdentificacion(TipoIdentificacion tipo){

		return ifTipoRepo.save(tipo);
	}
	
	public  String deleteTipoIdentificacionId(Long id){

		String resultado;
		try {
			ifTipoRepo.deleteById(id);
			resultado="Registro elimminado con éxito.";
		}catch(Exception e){
			resultado="No se pudo eliminar el registro.";
		}
			
		return resultado;
		 
	}
	
	public  TipoIdentificacion updateTipoIdentificacion(TipoIdentificacion tipo){

		return ifTipoRepo.save(tipo);
	}

}
