package com.test.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.crud.model.entity.TipoIdentificacion;

public interface IFTipoIdentificacion extends JpaRepository<TipoIdentificacion, Long>{

}
