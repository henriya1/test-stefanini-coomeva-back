package com.test.crud.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.crud.model.entity.Roles;

@Repository("iFRolesRepository")
public interface IFRolesRepository extends JpaRepository<Roles, Long>{
	public abstract Roles findByUsuario(String usuario);	
}