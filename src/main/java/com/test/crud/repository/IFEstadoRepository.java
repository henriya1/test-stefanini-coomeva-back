package com.test.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.crud.model.entity.Estado;

public interface IFEstadoRepository extends JpaRepository<Estado, Long>{

	
	
}
